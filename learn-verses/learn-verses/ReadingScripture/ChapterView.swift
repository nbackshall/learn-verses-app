//
//  ChapterView.swift
//  learn-verses
//
//  Created by Nicholas Backshall on 10/05/2022.
//

import SwiftUI

struct ChapterView: View {
    
    @ObservedObject var viewModel: BookSelectionViewModel
    
    var body: some View {
        ScrollView {
            VStack {
                ForEach(viewModel.verses, id: \.self) { verse in
                    Text(verse.text)
                }
            }
        }
    }
}

//struct ChapterView_Previews: PreviewProvider {
//    static var previews: some View {
////        ChapterView()
//    }
//}
