//
//  Bible.swift
//  learn-verses
//
//  Created by Nicholas Backshall on 20/09/2020.
//

import Foundation
import SwiftUI

struct Bible {
    private let item: BiblesResponse.BibleResponse
    
    var name: String? {
        return item.name
    }
    
    init(item: BiblesResponse.BibleResponse) {
      self.item = item
    }
}

extension Bible: Hashable {
    static func == (lhs: Bible, rhs: Bible) -> Bool {
        return lhs.name == rhs.name
    }
    
    func hash(into hasher: inout Hasher) {
      hasher.combine(self.name)
    }
}
