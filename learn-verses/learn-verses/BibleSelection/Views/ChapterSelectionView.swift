//
//  ChapterSelectionView.swift
//  learn-verses
//
//  Created by Nicholas Backshall on 11/05/2022.
//

import SwiftUI

struct ChapterSelectionView: View {
    @ObservedObject var viewModel: BookSelectionViewModel
    @State var shouldShowVerses: Bool = false
    
    @Environment(\.managedObjectContext) var moc
    
    var body: some View {
        if viewModel.isLoadingVerses {
            ProgressView()
                .navigationBarTitle("")
                .navigationBarHidden(true)
        } else {
            ZStack(alignment: .bottom) {
                ScrollView(showsIndicators: false) {
                    VStack(spacing: 0) {
                        
                        let chapterName = viewModel.selectedChapter?.name ?? ""
                        
                        if viewModel.shouldShowBookName {
                            VStack(spacing: Font.bookNameLineWidth * 0.3) {
                                Text(viewModel.selectedBookName)
                                    .font(Font(forType: .book))
                                Rectangle()
                                    .fill(.gray)
                                    .frame(width: Font.bookNameLineWidth, height: 1.5, alignment: .center)
                            }
                            .padding(.bottom, Font.bookNameLineWidth)
                        } else {
                            Text(viewModel.selectedBookName)
                                .font(Font(forType: .verseNumber))
                                .frame(maxWidth: .infinity, alignment: .center)
                                .padding(.vertical, Font.bookNameLineWidth * 0.1)
                        }
                        
                        Text(chapterName)
                            .font(Font(forType: .chapter))
                            .frame(maxWidth: .infinity, alignment: .leading)
                            .padding(.horizontal, Font.bookNameLineWidth * 0.3)
                        
                        viewModel.verses.reduce(Text("\n")) { (result: Text, verse: VerseEntity) in
                            result
                            +
                            Text(verse.prefix)
                                .font(Font(forType: .verse))
                            +
                            Text("\(verse.number) ")
                                .font(Font(forType: .verseNumber))
                                .baselineOffset(Font.publicoVerseNumberBaselineOffset)
                                .foregroundColor(.gray)
                            +
                            Text("\(verse.text)")
                                .font(Font(forType: .verse))
                        }
                        .padding(.horizontal, Font.bookNameLineWidth * 0.3)
                        .multilineTextAlignment(.leading)
                        .lineSpacing(3.0)
                    }
                    .padding(.top, Font.bookNameLineWidth * 0.5)
                }
                
                ZStack(alignment: .bottom) {
                    HStack(alignment: .center, spacing: 10.0) {
                        Button("PREV") {
                            viewModel.getAdjacentChapter(isBefore: true)
                        }
                        .disabled(viewModel.isPreviousButtonDisabled)
                        .padding([.leading, .vertical], Font.bookNameLineWidth * 0.2)
                        
                        Divider()
                            .padding(.vertical, Font.bookNameLineWidth * 0.1)
                        
                        Button("NEXT") {
                            viewModel.getAdjacentChapter(isBefore: false)
                        }
                        .disabled(viewModel.isNextButtonDisabled)
                        .padding([.trailing, .vertical], Font.bookNameLineWidth * 0.2)
                    }
                    .background(.regularMaterial, in: RoundedRectangle(cornerRadius: Font.bookNameLineWidth * 0.25))
                    .frame(height: Font.bookNameLineWidth * 0.5, alignment: .bottom)
                    .padding()
                }
                
            }
            .navigationBarTitle("")
            .navigationBarHidden(true)
        }
    }
}

//struct ChapterSelection_Previews: PreviewProvider {
//    static var previews: some View {
//        ChapterSelection()
//    }
//}

