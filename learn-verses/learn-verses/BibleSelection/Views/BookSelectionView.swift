//
//  BookSelectionView.swift
//  learn-verses
//
//  Created by Nic Backshall on 12/11/2020.
//

import SwiftUI
import CoreData

struct BookSelectionView: View {
    
    @ObservedObject var viewModel: BookSelectionViewModel
    
    @Environment(\.managedObjectContext) var moc
    
    @FetchRequest(sortDescriptors: [
        SortDescriptor(\.position)
    ]) var books: FetchedResults<BookEntity>
    
    @FetchRequest(sortDescriptors: []) var bibles: FetchedResults<BibleEntity>
    
    var body: some View {
        return GeometryReader { geometry in
            NavigationView {
                ScrollView(showsIndicators: false) {
                    VStack(spacing: 0.0) {
                        ForEach(bibles, id: \.self) { bible in
                            ForEach(bible.bookEntityArray, id: \.self) { book in
                                NavigationLink(destination:
                                    ChapterSelectionView(viewModel: viewModel)
                                        .environment(\.managedObjectContext, self.moc)
                                        .onAppear {
                                            guard let chapter = book.chapterEntityArray.first else {
                                                print("Chapter Entity Array is empyty")
                                                return
                                            }
                                            viewModel.selectChapter(withObjectId: chapter.objectID)
                                        }
                                   )
                                {
                                    BookView(book: book, size: geometry.size)
                                        .offset(x: Double.random(in: -1.0...1.0) * geometry.size.width * 0.07)
                                        .scaleEffect(CGSize(width: Double.random(in: 0...1.0) > 2.0 ? -1.0 : 1.0, height: 1.0))
                                        .zIndex(Double(66 - Int(book.position)))
                                }
                                .isDetailLink(false)
                                .navigationBarTitle("")
                                .navigationBarHidden(true)
                            }
                        }
                    }
                    .frame(width: geometry.size.width)
                }
            }
            .background(Color.gray)
            .ignoresSafeArea()
            .onAppear {
                guard let bible = bibles.first, bible.hasSavedAllBooks else {
                    viewModel.testFetchBooks(moc: moc)
                    return
                }
            }
        }
    }
}

struct BookSelectionView_Previews: PreviewProvider {
    static var previews: some View {
        BookSelectionView(viewModel: BookSelectionViewModel())
    }
}
