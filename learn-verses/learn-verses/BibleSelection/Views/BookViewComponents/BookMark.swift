//
//  BookMark.swift
//  learn-verses
//
//  Created by Nicholas Backshall on 06/02/2022.
//

import SwiftUI

struct BookMark: Shape {
    var angle: Double
    var thickness: CGFloat
    
    func path(in rect: CGRect) -> Path {
        let bookAngle = angle * Double.pi / 180
        let paperLength = CGFloat(Double(rect.width) * 0.707 * tan(bookAngle))
        
        var path = Path()
        
        var numberOfLines = Int(floor((rect.height - 2 * thickness) / CGFloat(7)))
        if numberOfLines % 2 == 0 {
            numberOfLines += 1
        }
        
        let lineGap = (rect.height - 2 * thickness) / CGFloat(numberOfLines + 1)
        
        let startX = paperLength * 0.7
        let startY = 3 * lineGap
        let width = paperLength * 0.1
        let length = 4 * lineGap
        let point = 0.7 * length
        let twist = 0.1
        let firstBend = length * 0.5
        let secondBend = length * 0.7
        
        path.move(to: CGPoint(x: rect.minX + startX, y: rect.minY + thickness + startY))
        path.addCurve(
            to: CGPoint(x: rect.minX + startX, y: rect.minY + thickness + startY + length),
            control1: CGPoint(x: rect.minX + startX * CGFloat(1 - twist), y: rect.minY + startY + firstBend),
            control2: CGPoint(x: rect.minX + startX * CGFloat(1 + twist), y: rect.minY + startY + secondBend)
        )
        path.addQuadCurve(
            to: CGPoint(x: rect.minX + startX - (width / 2) * CGFloat(1 - twist), y: rect.minY + thickness + startY + point),
            control: CGPoint(x: rect.minX + startX - width * CGFloat(0.25), y: rect.minY + thickness + startY + ((length + point) / 2) * CGFloat((1 - twist)))
        )
        path.addQuadCurve(
            to: CGPoint(x: rect.minX + startX - width, y: rect.minY + thickness + startY + length),
            control: CGPoint(x: rect.minX + startX - width * CGFloat(0.75), y: rect.minY + thickness + startY + ((length + point) / 2) * CGFloat((1 + twist)))
        )
        path.addCurve(
            to: CGPoint(x: rect.minX + startX - width, y: rect.minY + thickness + startY),
            control1: CGPoint(x: rect.minX + startX * CGFloat(1 + twist) - width , y: rect.minY + startY + firstBend),
            control2: CGPoint(x: rect.minX + startX * CGFloat(1 - twist) - width, y: rect.minY + startY + secondBend)
        )
        path.addLine(to: CGPoint(x: rect.minX + startX, y: rect.minY + thickness + startY))
        
        return path
    }
    
    func adjustAngle(angle: Angle) -> Angle {
        return angle - Angle.degrees(90)
    }
}
