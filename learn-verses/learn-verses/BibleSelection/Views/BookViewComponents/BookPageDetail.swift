//
//  BookPageDetail.swift
//  learn-verses
//
//  Created by Nicholas Backshall on 06/02/2022.
//

import SwiftUI

struct BookPageDetail: Shape {
    var angle: Double
    var thickness: CGFloat
    
    func path(in rect: CGRect) -> Path {
        let bookAngle = angle * Double.pi / 180
        let paperLength = CGFloat(Double(rect.width) * 0.707 * tan(bookAngle))
        let paperIndent = CGFloat(Double(rect.width) * 0.08 * tan(bookAngle))
        
        var path = Path()
        
        var numberOfLines = Int(floor((rect.height - 2 * thickness) / CGFloat(7)))
        if numberOfLines % 2 == 0 {
            numberOfLines += 1
        }
        
        let lineGap = (rect.height - 2 * thickness) / CGFloat(numberOfLines + 1)
        
        if numberOfLines >= 1 {
            path.move(to: CGPoint(x: rect.minX + paperIndent, y: rect.minY + thickness))
            
            for i in 1...numberOfLines {
                path.move(to: CGPoint(
                    x: rect.minX + paperIndent,
                    y: rect.minY + thickness + CGFloat(i) * lineGap
                ))
                path.addLine(to: CGPoint(
                    x: rect.minX + paperLength - thickness,
                    y: rect.minY + thickness + CGFloat(i) * lineGap
                ))
            }
        }
        
        return path
    }
    
    func adjustAngle(angle: Angle) -> Angle {
        return angle - Angle.degrees(90)
    }
}
