//
//  BookShadow.swift
//  learn-verses
//
//  Created by Nicholas Backshall on 06/02/2022.
//

import SwiftUI

struct BookShadow: Shape {
    var angle: Double
    
    func path(in rect: CGRect) -> Path {
        let cornerRadius = rect.width * 0.05
        let bookAngle = angle * Double.pi / 180
        let paperLength = CGFloat(Double(rect.width) * 0.707 * tan(bookAngle))
        
        let shadowY = cornerRadius * 0.4
        let arcAngle = acos((Double(cornerRadius) - Double(shadowY)) / Double(cornerRadius))
        let shadowX = CGFloat(cornerRadius - CGFloat(sin(arcAngle)) * shadowY)
        
        var path = Path()
        
        path.move(to: CGPoint(x: paperLength - cornerRadius, y: rect.maxY))
        path.addLine(to: CGPoint(x: rect.maxX - cornerRadius, y: rect.maxY))
        path.addArc(
            center: CGPoint(x: rect.maxX - cornerRadius, y: rect.maxY - cornerRadius),
            radius: cornerRadius,
            startAngle: adjustAngle(angle: .degrees(180)),
            endAngle: adjustAngle(angle: .degrees(90)),
            clockwise: true)
        path.addLine(to: CGPoint(
                        x: rect.maxX,
                        y: rect.maxY - cornerRadius - shadowY
        ))
        path.addArc(
            center: CGPoint(x: rect.maxX - cornerRadius, y: rect.maxY - cornerRadius - shadowY),
            radius: cornerRadius,
            startAngle: adjustAngle(angle: .degrees(90)),
            endAngle: adjustAngle(angle: .degrees(180)),
            clockwise: false)
        path.addLine(to: CGPoint(
                        x: paperLength - shadowX,
                        y: rect.maxY - shadowY
        ))
        path.addArc(
            center: CGPoint(x: paperLength - cornerRadius, y: rect.maxY - cornerRadius),
            radius: cornerRadius,
            startAngle: adjustAngle(angle: .radians(Double.pi - arcAngle)),
            endAngle: adjustAngle(angle: .degrees(180)),
            clockwise: false)
        
        return path
    }
    
    func adjustAngle(angle: Angle) -> Angle {
        return angle - Angle.degrees(90)
    }
}
