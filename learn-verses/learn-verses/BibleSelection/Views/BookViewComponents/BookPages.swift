//
//  BookPages.swift
//  learn-verses
//
//  Created by Nicholas Backshall on 06/02/2022.
//

import SwiftUI

struct BookPages: Shape {
    var angle: Double
    var thickness: CGFloat
    
    func path(in rect: CGRect) -> Path {
        let outerCornerRadius = rect.width * 0.05
        let bookAngle = angle * Double.pi / 180
        let paperLength = CGFloat(Double(rect.width) * 0.707 * tan(bookAngle))
        let paperIndent = CGFloat(Double(rect.width) * 0.08 * tan(bookAngle))
        
        let innerCornerRadius = outerCornerRadius - thickness
        
        var path = Path()
        
        path.move(to: CGPoint(x: rect.minX + paperIndent, y: rect.minY + thickness))
        path.addLine(to: CGPoint(
                        x: rect.minX + paperLength - innerCornerRadius - thickness,
                        y: rect.minY + thickness
        ))
        path.addArc(
            center: CGPoint(
                x: rect.minX + paperLength - innerCornerRadius - thickness,
                y: rect.minY + thickness + innerCornerRadius
            ),
            radius: innerCornerRadius,
            startAngle: adjustAngle(angle: .degrees(0)),
            endAngle: adjustAngle(angle: .degrees(90)),
            clockwise: false)
        path.addLine(to: CGPoint(
                        x: rect.minX + paperLength - thickness,
                        y: rect.maxY - thickness - innerCornerRadius
        ))
        path.addArc(
            center: CGPoint(
                x: rect.minX + paperLength - innerCornerRadius - thickness,
                y: rect.maxY - thickness - innerCornerRadius
            ),
            radius: innerCornerRadius,
            startAngle: adjustAngle(angle: .degrees(90)),
            endAngle: adjustAngle(angle: .degrees(180)),
            clockwise: false)
        path.addLine(to: CGPoint(x: rect.minX + paperIndent, y: rect.maxY - thickness))
        path.addLine(to: CGPoint(x: rect.minX + paperIndent, y: rect.minY + thickness))
        
        return path
    }
    
    func adjustAngle(angle: Angle) -> Angle {
        return angle - Angle.degrees(90)
    }
}
