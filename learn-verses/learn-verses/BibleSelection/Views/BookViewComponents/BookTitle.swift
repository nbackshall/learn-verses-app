//
//  BookTitle.swift
//  learn-verses
//
//  Created by Nicholas Backshall on 06/02/2022.
//

import SwiftUI

struct BookTitle: View {
    
    var title: String
    var height: CGFloat
    var width: CGFloat
    let textSize: CGFloat = 400
    let fontName: String = "TimesNewRomanPS-BoldMT"
    
    init(title: String, angle: Double, width: CGFloat, height: CGFloat) {
        self.title = title.uppercased()
        self.height = height
        self.width = width
    }
    
    var body: some View {
        HStack(alignment: .center) {
            Text(title)
                .foregroundColor(.white)
                .font(.custom(fontName, size: textSize))
                .fixedSize()
                .lineLimit(1)
                .scaleEffect(CGSize(
                    width: width / (title.widthOfString(usingFont: UIFont(name: fontName, size: textSize)) ?? width),
                    height: height / (title.heightOfString(usingFont: UIFont(name: fontName, size: textSize)) ?? height)
                ))
        }
    }
}
