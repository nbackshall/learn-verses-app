//
//  BookSpine.swift
//  learn-verses
//
//  Created by Nicholas Backshall on 06/02/2022.
//

import SwiftUI

struct BookSpine: Shape {
    var angle: Double
    
    func path(in rect: CGRect) -> Path {
        let cornerRadius = rect.width * 0.05
        let bookAngle = angle * Double.pi / 180
        let paperLength = CGFloat(Double(rect.width) * 0.707 * tan(bookAngle))
        
        var path = Path()
        
        path.move(to: CGPoint(x: paperLength - cornerRadius, y: rect.minY))
        path.addLine(to: CGPoint(x: rect.maxX - cornerRadius, y: rect.minY))
        path.addArc(
            center: CGPoint(x: rect.maxX - cornerRadius, y: rect.minY + cornerRadius),
            radius: cornerRadius,
            startAngle: adjustAngle(angle: .degrees(0)),
            endAngle: adjustAngle(angle: .degrees(90)),
            clockwise: false)
        path.addLine(to: CGPoint(x: rect.maxX, y: rect.maxY - cornerRadius))
        path.addArc(
            center: CGPoint(x: rect.maxX - cornerRadius, y: rect.maxY - cornerRadius),
            radius: cornerRadius,
            startAngle: adjustAngle(angle: .degrees(90)),
            endAngle: adjustAngle(angle: .degrees(180)),
            clockwise: false)
        path.addLine(to: CGPoint(x: paperLength - cornerRadius, y: rect.maxY))
        path.addArc(
            center: CGPoint(x: paperLength - cornerRadius, y: rect.maxY - cornerRadius),
            radius: cornerRadius,
            startAngle: adjustAngle(angle: .degrees(180)),
            endAngle: adjustAngle(angle: .degrees(90)),
            clockwise: true)
        path.addLine(to: CGPoint(x: paperLength, y: rect.minY + cornerRadius))
        path.addArc(
            center: CGPoint(x: paperLength - cornerRadius, y: rect.minY + cornerRadius),
            radius: cornerRadius,
            startAngle: adjustAngle(angle: .degrees(90)),
            endAngle: adjustAngle(angle: .degrees(0)),
            clockwise: true)
        path.addLine(to: CGPoint(x: paperLength - cornerRadius, y: rect.minY))
        
        return path
    }
    
    func adjustAngle(angle: Angle) -> Angle {
        return angle - Angle.degrees(90)
    }
}
