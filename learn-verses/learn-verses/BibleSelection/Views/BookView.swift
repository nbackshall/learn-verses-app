//
//  BookView.swift
//  learn-verses
//
//  Created by Nic Backshall on 12/11/2020.
//

import SwiftUI

struct BookView: View {
    
    var angle: Double
    var colour: Color
    var width: CGFloat
    var height: CGFloat
    var thickness: CGFloat
    
    let title: String
    
    init(book: BookEntity, size: CGSize) {
        self.angle = Double.random(in: 10..<25)
        self.colour = BookViewHelper.color(from: book.name)
        self.width = size.width * 0.6
        self.height = BookViewHelper.calculateHeight(chapterCount: Int(book.chapterCount), size: size)
        self.thickness = size.width * 0.01
        
        self.title = book.name.uppercased()
    }
    
    var body: some View {
        ZStack {
            BookPages(angle: angle, thickness: thickness)
                .fill(Color.Book.pageLight)
                .frame(width: width, height: height, alignment: .center)
            
            BookPageDetail(angle: angle, thickness: thickness)
                .stroke(Color.Book.pageDark, lineWidth: CGFloat(2))
                .frame(width: width, height: height, alignment: .center)
            
            BookSpine(angle: angle)
                .fill(colour)
                .frame(width: width, height: height, alignment: .center)
            
            BookEdge(angle: angle, thickness: thickness)
                .fill(colour)
                .brightness(0.15)
                .frame(width: width, height: height, alignment: .center)
            
            BookShadow(angle: angle)
                .fill(colour)
                .brightness(-0.1)
                .frame(width: width, height: height, alignment: .center)
            
            if Double.random(in: 0...1.0) > 0.8 {
                BookMark(angle: angle, thickness: thickness)
                    .fill(colour)
                    .brightness(-0.25)
                    .frame(width: width, height: height, alignment: .center)
            }
            
            VStack(alignment: .trailing) {
                BookTitle(
                    title: title, angle: angle, width: width - CGFloat(Double(width) * 0.707 * tan(angle * Double.pi / 180)) * 1.2, height: height)
                    .frame(
                        width: width - CGFloat(Double(width) * 0.707 * tan(angle * Double.pi / 180)),
                        height: height
                    )
            }
            .frame(width: width, height: height, alignment: .trailing)
        }
    }
}

//struct BookView_Previews: PreviewProvider {
//    typealias ChapterSummaryResponse = BooksResponse.BookResponse.ChapterSummaryResponse
//    
//    static var previews: some View {
//        ZStack {
//            VStack {
//                BookView(
//                    book: Book(response:
//                                BookResponse(
//                                    abbreviation: "abc",
//                                    id: "abc",
//                                    bibleId: "abc",
//                                    name: "Genesis",
//                                    nameLong: "abc",
//                                    chapters: Array(repeating: ChapterSummaryResponse(id: "abc", bibleId: "abc", bookId: "abc", number: String(1), position: Int64(1)), count: 60)
//                                ),
//                               selectedBibleId: "abc"
//                              ),
//                    size: CGSize(width: 500.0, height: 0.0)
//                )
//                BookView(
//                    book: Book(response:
//                                BookResponse(
//                                    abbreviation: "abc",
//                                    id: "abc",
//                                    bibleId: "abc",
//                                    name: "Mathew",
//                                    nameLong: "abc",
//                                    chapters: Array(repeating: ChapterSummaryResponse(id: "abc", bibleId: "abc", bookId: "abc", number: String(1), position: Int64(1)), count: 30)
//                                ),
//                               selectedBibleId: "abc"
//                              ),
//                    size: CGSize(width: 500.0, height: 0.0)
//                )
//                BookView(
//                    book: Book(response:
//                                BookResponse(
//                                    abbreviation: "abc",
//                                    id: "abc",
//                                    bibleId: "abc",
//                                    name: "1 Corinthians",
//                                    nameLong: "abc",
//                                    chapters: Array(repeating: ChapterSummaryResponse(id: "abc", bibleId: "abc", bookId: "abc", number: String(1), position: Int64(1)), count: 500)
//                                ),
//                               selectedBibleId: "abc"
//                              ),
//                    size: CGSize(width: 500.0, height: 0.0)
//                )
//            }
//        }.frame(width: 200, height: 500, alignment: .center)
//            .previewLayout(.fixed(width: 800, height: 1000))
//    }
//}
