//
//  BookSelectionViewModel.swift
//  learn-verses
//
//  Created by Nic Backshall on 19/11/2020.
//

import Foundation
import Combine
import SwiftUI
import CoreData

class BookSelectionViewModel: ObservableObject {
    
    var moc = PersistenceController.shared.container.viewContext
    
    var selectedBibleName = "Free Bible Version"
    var selectedBibleId = "65eec8e0b60e656b-01"
    
    @Published var selectedBook: BookEntity? = nil
    @Published var previousChapter: ChapterEntity? = nil
    @Published var selectedChapter: ChapterEntity? = nil
    @Published var nextChapter: ChapterEntity? = nil
    @Published var verses: [VerseEntity] = []
    @Published var selectedBookName: String = ""
    @Published var shouldShowBookName: Bool = false
    @Published var isPreviousButtonDisabled: Bool = true
    @Published var isNextButtonDisabled: Bool = true
    @Published var isLoadingVerses: Bool = true
    
    private var cancellables = CancellableBag()
    
    let persistenceController = PersistenceController.shared
    
    public var chapters: [ChapterEntity] {
        selectedBook?.chapterEntityArray ?? []
    }
    
    init() {
        $previousChapter
            .sink { [weak self] previousChapter in
                guard let self = self, let previousChapter = previousChapter else {
                    return
                }
                if previousChapter.hasSavedAllVerses {
                    self.isPreviousButtonDisabled = false
                    return
                } else {
                    self.fetchChapter(withEntity: previousChapter, completion: { _ in self.isPreviousButtonDisabled = false })
                }
            }
            .store(in: &cancellables)
        
        $selectedChapter
            .sink { [weak self] selectedChapter in
                guard let self = self, let selectedChapter = selectedChapter else {
                    return
                }
                self.selectedBookName = selectedChapter.book?.name ?? ""
                self.shouldShowBookName = selectedChapter.number == 1
                self.previousChapter = selectedChapter.previousChapter
                self.nextChapter = selectedChapter.nextChapter
                if selectedChapter.hasSavedAllVerses {
                    self.verses = selectedChapter.verseEntityArray
                    self.isLoadingVerses = false
                } else {
                    self.fetchChapter(withEntity: selectedChapter, completion: { newChapterEntityId in
                        guard let newChapterEntity = self.moc.object(with: newChapterEntityId) as? ChapterEntity else {
                            print("No object found")
                            return
                        }
                        self.verses = newChapterEntity.verseEntityArray
                        self.isLoadingVerses = false
                    })
                }
            }
            .store(in: &cancellables)
        
        $nextChapter
            .sink { [weak self] nextChapter in
                guard let self = self, let nextChapter = nextChapter else {
                    return
                }
                if nextChapter.hasSavedAllVerses {
                    self.isNextButtonDisabled = false
                    return
                } else {
                    self.fetchChapter(withEntity: nextChapter, completion: { _ in self.isNextButtonDisabled = false })
                }
            }
            .store(in: &cancellables)
    }
    
    func selectBook(withObjectId objectId: NSManagedObjectID) {
        guard let book = moc.object(with: objectId) as? BookEntity else {
            print("No object found")
            return
        }
        selectedBook = book
    }
    
    func selectChapter(withObjectId objectId: NSManagedObjectID) {
        guard let chapter = moc.object(with: objectId) as? ChapterEntity else {
            print("No object found")
            return
        }
        isLoadingVerses = true
        selectedChapter = chapter
    }
    
    private func fetchChapter(withEntity chapterEntity: ChapterEntity, completion: @escaping (NSManagedObjectID) -> ()) {
        return BibleFetcher().fetchChapter(withBibleId: "65eec8e0b60e656b-01", withChapterId: chapterEntity.id)
            .receive(on: DispatchQueue.main)
            .sink(
                receiveCompletion: { value in
                    switch value {
                    case .failure:
                        print("Failed to get chapter")
                    case .finished:
                        break
                    }
                },
                receiveValue: { [weak self] chapterResponse in
                    guard let self = self else { return }
                    guard let updatedChapterEntityObjectId = self.persistenceController.save(chapterResponse: chapterResponse, to: chapterEntity) else {
                        print("Unable to update chapter entity")
                        return
                    }
                    completion(updatedChapterEntityObjectId)
                })
            .store(in: &cancellables)
    }
    
    func testFetchBooks(moc: NSManagedObjectContext) {
        BibleFetcher().fetchBooks(withBibleId: selectedBibleId)
            .receive(on: DispatchQueue.main)
            .sink(
                receiveCompletion: { value in
                    switch value {
                    case .failure:
                        print("Failed to get books")
                    case .finished:
                        break
                    }
                },
                receiveValue: { [weak self] books in
                    guard let self = self else { return }
                    self.persistenceController.createBible(forId: self.selectedBibleId, withBooks: books, moc: moc)
                })
            .store(in: &cancellables)
    }
    
    func getAdjacentChapter(isBefore: Bool) {
        guard let chapter = selectedChapter else {
            print("Selected Chapter is nil")
            return
        }
        guard let newChapter = isBefore ? chapter.previousChapter : chapter.nextChapter else {
            print(isBefore ? "Previous chapter does not exist" : "Next chapter does not exist")
            return
        }
        isPreviousButtonDisabled = true
        isNextButtonDisabled = true
        selectedChapter = newChapter
    }
}
