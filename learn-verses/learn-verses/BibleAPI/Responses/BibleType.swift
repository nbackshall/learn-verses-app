//
//  BibleType.swift
//  learn-verses
//
//  Created by Nic Backshall on 19/11/2020.
//

import Foundation

struct BibleType {
    let id: String
    let name: String
    let books: [BookType]
    
    struct BookType {
        let id: String
        let name: String
        let chapters: [ChapterType]
        
        struct ChapterType {
            let id: String
            let name: String
        }
    }
}
