//
//  Responses.swift
//  learn-verses
//
//  Created by Nicholas Backshall on 20/09/2020.
//

import Foundation

struct BiblesResponse: Codable {
    let data: [BibleResponse]
    
    struct BibleResponse: Codable {
        let id: String?
        let dblId: String?
        let relatedDbl: String?
        let name: String?
        let nameLocal: String?
        let abbreviation: String?
        let abreviationLocal: String?
        let description: String?
        let descriptionLocal: String?
        let language: LanguageResponse?
        let Countries: CountriesResponse?
        let type: String?
        let updatedAt: String?
        let audioBibles: [AudioBiblesResponse?]
    }
}

struct BooksResponse: Codable {
    let data: [BookResponse]
    
    struct BookResponse: Codable {
        let abbreviation: String
        let id: String
        let bibleId: String
        let name: String
        let nameLong: String
        let chapters: [ChapterSummaryResponse]
        
        struct ChapterSummaryResponse: Codable {
            let id: String
            let bibleId: String
            let bookId: String
            let number: String
            let position: Int64
        }
    }
}

struct ChaptersResponse: Codable {
    let data: [ChapterSummaryResponse]
    
    struct ChapterSummaryResponse: Codable {
        let id: String
        let bibleId: String
        let bookId: String
        let number: String
        let reference: String
    }
}

struct LanguageResponse: Codable {
    let id: String?
    let name: String?
    let nameLocal: String?
    let script: String?
    let scriptDirection: String?
}

struct CountriesResponse: Codable {
    let id: String?
    let name: String?
    let nameLocal: String?
}

struct AudioBiblesResponse: Codable {
    let id: String?
    let name: String?
    let nameLocal: String?
    let description: String?
    let descriptionLocal: String?
}
