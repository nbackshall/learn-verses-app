//
//  PassageResponses.swift
//  learn-verses
//
//  Created by Nicholas Backshall on 08/05/2022.
//

import Foundation

struct PassageResponse: Codable {
    let data: Details
    let meta: MetaDetails
    
    struct Details: Codable {
        let id: String
        let orgId: String
        let bibleId: String
        let bookId: String
        let chapterIds: [String]
        let reference: String
        let content: [Content]
        let verseCount: Int64
        let copyright: String
        
        enum Content: Codable {
            case chapter(ChapterContent)
            case paragraph(ParagraphContent)
            
            private enum CodingKeys: String, CodingKey {
                case name = "name"
            }
            
            init(from decoder: Decoder) throws {
                let container = try decoder.container(keyedBy: CodingKeys.self)
                let singleContainer = try decoder.singleValueContainer()
                
                let name = try container.decode(String.self, forKey: .name)
                switch name {
                case "chapter":
                    let chapter = try singleContainer.decode(ChapterContent.self)
                    self = .chapter(chapter)
                case "para":
                    let paragraph = try singleContainer.decode(ParagraphContent.self)
                    self = .paragraph(paragraph)
                default:
                    fatalError("Unknown type of content.")
                    // or handle this case properly
                }
            }
            
            func encode(to encoder: Encoder) throws {
                var singleContainer = encoder.singleValueContainer()
                
                switch self {
                case .chapter(let chapter):
                    try singleContainer.encode(chapter)
                case .paragraph(let paragraph):
                    try singleContainer.encode(paragraph)
                }
            }
        }
    }
    
    struct MetaDetails: Codable {
        let fums: String
        let fumsId: String
        let fumsJsInclude: String
        let fumsJs: String
        let fumsNoScript: String
    }
}

struct ChapterContent: Codable {
    let name: String
    let type: String
    let attrs: NumberAttributes
    let items: [SimpleTextItem]
}

struct ParagraphContent: Codable {
    let name: String
    let type: String
    let attrs: ParagraphAttributes
    let items: [ParagraphItem] // Text or Tag
    
    enum ParagraphItem: Codable {
        case tag(TagItem)
        case text(TextItem)
        
        private enum CodingKeys: String, CodingKey {
            case type = "type"
        }
        
        init(from decoder: Decoder) throws {
            let container = try decoder.container(keyedBy: CodingKeys.self)
            let singleContainer = try decoder.singleValueContainer()
            
            let type = try container.decode(String.self, forKey: .type)
            switch type {
            case "tag":
                let tag = try singleContainer.decode(TagItem.self)
                self = .tag(tag)
            case "text":
                let text = try singleContainer.decode(TextItem.self)
                self = .text(text)
            default:
                fatalError("Unknown type of content.")
                // or handle this case properly
            }
        }
        
        func encode(to encoder: Encoder) throws {
            var singleContainer = encoder.singleValueContainer()
            
            switch self {
            case .tag(let tag):
                try singleContainer.encode(tag)
            case .text(let text):
                try singleContainer.encode(text)
            }
        }
    }
    
    struct TextItem: Codable {
        let text: String
        let type: String
        let attrs: VerseAttributes
        
        struct VerseAttributes: Codable {
            let verseId: String
            let verseOrgIds: [String]
        }
    }

    struct TagItem: Codable {
        let name: String
        let type: String
        let attrs: NumberAttributes
        let items: [SimpleTextItem]
    }
}

struct NumberAttributes: Codable {
    let number: String
    let style: String
    let sid: String
}

struct ParagraphAttributes: Codable {
    let style: String
}

struct SimpleTextItem: Codable {
    let text: String
    let type: String
}

struct ChapterResponse: Codable {
    let data: Details
    let meta: MetaDetails

    struct Details: Codable {
        let id: String
        let bibleId: String
        let number: String
        let bookId: String
        let content: [ParagraphContent]
        let reference: String
        let verseCount: Int64
        let next: Adjacent?
        let previous: Adjacent?
        let copyright: String
        
        struct Adjacent: Codable {
            let id: String
            let bookId: String
            let number: String
        }
    }
    
    struct MetaDetails: Codable {
        let fums: String
        let fumsId: String
        let fumsJsInclude: String
        let fumsJs: String
        let fumsNoScript: String
    }
}

