//
//  BibleFetcher.swift
//  learn-verses
//
//  Created by Nicholas Backshall on 20/09/2020.
//

import Foundation
import Combine

protocol BibleFetchable {
    func fetchBibles() -> AnyPublisher<BiblesResponse, BibleError>
    func fetchBooks(withBibleId bibleId: String) -> AnyPublisher<BooksResponse, BibleError>
    func fetchChapters(withBibleId bibleId: String, withBookId bookId: String) -> AnyPublisher<ChaptersResponse, BibleError>
    func fetchChapter(withBibleId bibleId: String, withChapterId bookId: String) -> AnyPublisher<ChapterResponse, BibleError>
    func fetchPassage(withBibleId bibleId: String, withPassageId passageId: String) -> AnyPublisher<PassageResponse, BibleError>
}

class BibleFetcher {
    private let session: URLSession
    
    init() {
        let config = URLSessionConfiguration.default
        config.httpAdditionalHeaders = [
            "api-key": BibleAPI.key
        ]
        
        self.session = URLSession(configuration: config)
    }
}

extension BibleFetcher: BibleFetchable {
    func fetchBibles() -> AnyPublisher<BiblesResponse, BibleError> {
        return response(with: makeBiblesRequestComponents(), print: false)
    }
    
    func fetchBooks(withBibleId bibleId: String) -> AnyPublisher<BooksResponse, BibleError> {
        return fromFile(filename: "BibleBooks")
    }
    
    func fetchChapters(withBibleId bibleId: String, withBookId bookId: String) -> AnyPublisher<ChaptersResponse, BibleError> {
        return response(with: makeChaptersRequestComponents(withBibleId: bibleId, withBookId: bookId), print: false)
    }
    
    func fetchChapter(withBibleId bibleId: String, withChapterId chapterId: String) -> AnyPublisher<ChapterResponse, BibleError> {
        return response(with: makeChapterRequestComponents(withBibleId: bibleId, withChapterId: chapterId), print: false)
    }
    
    func fetchPassage(withBibleId bibleId: String, withPassageId passageId: String) -> AnyPublisher<PassageResponse, BibleError> {
        return response(with: makePassageRequestComponents(withBibleId: bibleId, withPassageId: passageId), print: true)
    }
    
    private func fromFile<T>(filename: String) -> AnyPublisher<T, BibleError> where T: Decodable {
        return Bundle.main.decodeable(filename: filename, withExtension: "json")
    }
    
    private func response<T>(
        with components: URLComponents, print p: Bool
    ) -> AnyPublisher<T, BibleError> where T: Decodable {
        guard let url = components.url else {
            let error = BibleError.network(description: "Couldn't create URL")
            return Fail(error: error).eraseToAnyPublisher()
        }
        return session.dataTaskPublisher(for: URLRequest(url: url))
            .tryMap { output -> Data in
                guard let response = output.response as? HTTPURLResponse, response.statusCode == 200 else {
                    throw HTTPError.statusCode
                }
                return output.data
            }
            .decode(type: T.self, decoder: JSONDecoder())
            .mapError { error in
                .network(description: error.localizedDescription)
            }
            .eraseToAnyPublisher()
    }
}

private extension BibleFetcher {
    struct BibleAPI {
      static let scheme = "https"
      static let host = "api.scripture.api.bible"
      static let path = "/v1"
      static let key = "a1b3a9ff39325dbf3be187fc22cdd29c"
    }
    
    func makeBiblesRequestComponents() -> URLComponents {
        var components = URLComponents()
        components.scheme = BibleAPI.scheme
        components.host = BibleAPI.host
        components.path = BibleAPI.path + "/bibles"
        
        components.queryItems = [
            URLQueryItem(name: "name", value: "free")
        ]
        
        return components
    }
    
    func makeBooksRequestComponents(withBibleId bibleId: String) -> URLComponents {
        var components = URLComponents()
        components.scheme = BibleAPI.scheme
        components.host = BibleAPI.host
        components.path = BibleAPI.path + "/bibles/\(bibleId)/books"
        
        components.queryItems = [
            URLQueryItem(name: "include-chapters", value: "true")
        ]
        
        return components
    }
    
    func makeChaptersRequestComponents(withBibleId bibleId: String, withBookId bookId: String) -> URLComponents {
        var components = URLComponents()
        components.scheme = BibleAPI.scheme
        components.host = BibleAPI.host
        components.path = BibleAPI.path + "/bibles/\(bibleId)/books/\(bookId)/chapters"
        
        components.queryItems = [
        ]
        
        return components
    }
    
    ///curl -X GET "https://api.scripture.api.bible/v1/bibles/65eec8e0b60e656b-01/chapters/GEN.1?
    ///content-type=json&
    ///include-notes=false&
    ///include-titles=true&
    ///include-chapter-numbers=false&
    ///include-verse-numbers=true&
    ///include-verse-spans=false"
    /// -H "accept: application/json" -H "api-key: a1b3a9ff39325dbf3be187fc22cdd29c"
    
    func makeChapterRequestComponents(withBibleId bibleId: String, withChapterId chapterId: String) -> URLComponents {
        var components = URLComponents()
        components.scheme = BibleAPI.scheme
        components.host = BibleAPI.host
        components.path = BibleAPI.path + "/bibles/\(bibleId)/chapters/\(chapterId)"
        
        components.queryItems = [
            URLQueryItem(name: "content-type", value: "json"),
            URLQueryItem(name: "include-notes", value: "false"),
            URLQueryItem(name: "include-titles", value: "true"),
            URLQueryItem(name: "include-chapter-numbers", value: "false"),
            URLQueryItem(name: "include-verse-numbers", value: "true"),
            URLQueryItem(name: "include-verse-spans", value: "false")
        ]
        
        return components
    }
    
    ///curl -X GET "https://api.scripture.api.bible/v1/bibles/65eec8e0b60e656b-01/passages/mat?
    ///content-type=html&
    ///include-notes=false&
    ///include-titles=true&
    ///include-chapter-numbers=true&
    ///include-verse-numbers=true&
    ///include-verse-spans=false&
    ///use-org-id=false"
    ///-H "accept: application/json" -H "api-key: a1b3a9ff39325dbf3be187fc22cdd29c"
    func makePassageRequestComponents(withBibleId bibleId: String, withPassageId passageId: String) -> URLComponents {
        var components = URLComponents()
        components.scheme = BibleAPI.scheme
        components.host = BibleAPI.host
        components.path = BibleAPI.path + "/bibles/\(bibleId)/passages/\(passageId)"
        
        components.queryItems = [
            URLQueryItem(name: "content-type", value: "json"),
            URLQueryItem(name: "include-notes", value: "false"),
            URLQueryItem(name: "include-titles", value: "true"),
            URLQueryItem(name: "include-chapter-numbers", value: "true"),
            URLQueryItem(name: "include-verse-numbers", value: "true"),
            URLQueryItem(name: "include-verse-spans", value: "false"),
            URLQueryItem(name: "use-org-id", value: "false")
        ]
        
        return components
    }
}
