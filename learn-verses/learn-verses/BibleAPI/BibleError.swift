//
//  BibleError.swift
//  learn-verses
//
//  Created by Nicholas Backshall on 20/09/2020.
//

import Foundation

enum BibleError: Error {
    case noFile(description: String)
    case parsing(description: String)
    case network(description: String)
}
