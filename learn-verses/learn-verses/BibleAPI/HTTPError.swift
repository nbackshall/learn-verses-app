//
//  HTTPError.swift
//  learn-verses
//
//  Created by Nicholas Backshall on 20/09/2020.
//

import Foundation

enum HTTPError: LocalizedError {
    case statusCode
    case post
}
