//
//  ContentView.swift
//  learn-verses
//
//  Created by Nicholas Backshall on 20/09/2020.
//

import SwiftUI

struct ContentView: View {
    
    @ObservedObject var viewModel: ContentViewModel
    
    init(viewModel: ContentViewModel) {
        self.viewModel = viewModel
    }
    
    var body: some View {
        List {
            ForEach(viewModel.biblesDataSource, id: \.self) { bible in
                Text(bible.name ?? "no name")
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView(viewModel: ContentViewModel(bibleFetcher: BibleFetcher()))
    }
}
