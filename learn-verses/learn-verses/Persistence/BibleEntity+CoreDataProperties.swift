//
//  BibleEntity+CoreDataProperties.swift
//  learn-verses
//
//  Created by Nicholas Backshall on 10/05/2022.
//
//

import Foundation
import CoreData


extension BibleEntity {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<BibleEntity> {
        return NSFetchRequest<BibleEntity>(entityName: "BibleEntity")
    }

    @NSManaged public var id: String
    @NSManaged public var bookCount: Int16
    @NSManaged public var name: String
    @NSManaged public var books: NSOrderedSet
    
    public var number: Int {
        if let n = RegexHelper.getFirstMatch(string: id, regex: "([0-9]+)$") {
            return Int(n) ?? -2
        } else {
            return -1
        }
    }
    
    public var bookEntityArray: [BookEntity] {
        let array = books.array as? [BookEntity] ?? []
        return array.sorted {
            $0.position < $1.position
        }
    }
    
    public var hasSavedAllBooks: Bool {
        bookCount == bookEntityArray.count && bookCount != 0
    }
}

// MARK: Generated accessors for books
extension BibleEntity {

    @objc(insertObject:inBooksAtIndex:)
    @NSManaged public func insertIntoBooks(_ value: BookEntity, at idx: Int)

    @objc(removeObjectFromBooksAtIndex:)
    @NSManaged public func removeFromBooks(at idx: Int)

    @objc(insertBooks:atIndexes:)
    @NSManaged public func insertIntoBooks(_ values: [BookEntity], at indexes: NSIndexSet)

    @objc(removeBooksAtIndexes:)
    @NSManaged public func removeFromBooks(at indexes: NSIndexSet)

    @objc(replaceObjectInBooksAtIndex:withObject:)
    @NSManaged public func replaceBooks(at idx: Int, with value: BookEntity)

    @objc(replaceBooksAtIndexes:withBooks:)
    @NSManaged public func replaceBooks(at indexes: NSIndexSet, with values: [BookEntity])

    @objc(addBooksObject:)
    @NSManaged public func addToBooks(_ value: BookEntity)

    @objc(removeBooksObject:)
    @NSManaged public func removeFromBooks(_ value: BookEntity)

    @objc(addBooks:)
    @NSManaged public func addToBooks(_ values: NSOrderedSet)

    @objc(removeBooks:)
    @NSManaged public func removeFromBooks(_ values: NSOrderedSet)

}

extension BibleEntity : Identifiable {

}
