//
//  BookEntity+CoreDataProperties.swift
//  learn-verses
//
//  Created by Nicholas Backshall on 08/05/2022.
//
//

import Foundation
import CoreData


extension BookEntity {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<BookEntity> {
        return NSFetchRequest<BookEntity>(entityName: "BookEntity")
    }

    @NSManaged public var id: String
    @NSManaged public var chapterCount: Int16
    @NSManaged public var name: String
    @NSManaged public var position: Int16
    @NSManaged public var chapters: NSOrderedSet
    @NSManaged public var bible: BibleEntity?
    
    public var chapterEntityArray: [ChapterEntity] {
        let array = chapters.array as? [ChapterEntity] ?? []
        return array.sorted {
            $0.number < $1.number
        }
    }
    
    public var hasSavedAllChapters: Bool {
        chapterCount == chapterEntityArray.count && chapterCount != 0
    }
    
    public var nextBook: BookEntity? {
        guard let bible = bible else {
            print("Bible is null, returning self")
            return nil
        }
        if position == (bible.bookCount - 1) {
            return nil
        } else {
            return bible.bookEntityArray[Int(position) + 1]
        }
    }
    
    public var previousBook: BookEntity? {
        if position == 0 {
            return nil
        } else {
            guard let bible = bible else {
                print("Bible is null, returning self")
                return nil
            }
            return bible.bookEntityArray[Int(position) - 1]
        }
    }
    
    public func isEqual(to other: BookEntity) -> Bool {
        return self.objectID === other.objectID
    }
}

// MARK: Generated accessors for chapters
extension BookEntity {

    @objc(insertObject:inChaptersAtIndex:)
    @NSManaged public func insertIntoChapters(_ value: ChapterEntity, at idx: Int)

    @objc(removeObjectFromChaptersAtIndex:)
    @NSManaged public func removeFromChapters(at idx: Int)

    @objc(insertChapters:atIndexes:)
    @NSManaged public func insertIntoChapters(_ values: [ChapterEntity], at indexes: NSIndexSet)

    @objc(removeChaptersAtIndexes:)
    @NSManaged public func removeFromChapters(at indexes: NSIndexSet)

    @objc(replaceObjectInChaptersAtIndex:withObject:)
    @NSManaged public func replaceChapters(at idx: Int, with value: ChapterEntity)

    @objc(replaceChaptersAtIndexes:withChapters:)
    @NSManaged public func replaceChapters(at indexes: NSIndexSet, with values: [ChapterEntity])

    @objc(addChaptersObject:)
    @NSManaged public func addToChapters(_ value: ChapterEntity)

    @objc(removeChaptersObject:)
    @NSManaged public func removeFromChapters(_ value: ChapterEntity)

    @objc(addChapters:)
    @NSManaged public func addToChapters(_ values: NSOrderedSet)

    @objc(removeChapters:)
    @NSManaged public func removeFromChapters(_ values: NSOrderedSet)

}

extension BookEntity : Identifiable {

}
