//
//  PersistenceController+Chapter.swift
//  learn-verses
//
//  Created by Nicholas Backshall on 10/05/2022.
//

import Foundation
import CoreData

extension PersistenceController {
    func save(chapterResponse: ChapterResponse, to chapterEntity: ChapterEntity) -> NSManagedObjectID? {
        let moc = PersistenceController.shared.container.viewContext
        let privateMoc = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
        privateMoc.parent = moc
        
        guard let chapter = privateMoc.object(with: chapterEntity.objectID) as? ChapterEntity else {
            print("No object found")
            return nil
        }
        
        guard let book = chapter.book else {
            print("chapterEntity.book is nil")
            return nil
        }
        
        let loggingEnabled = false
        
        let chapterName: String
        switch book.id {
        case "PSA":
            chapterName = "Psalm \(chapterResponse.data.number)"
        case "PRO":
            chapterName = "Proverb \(chapterResponse.data.number)"
        default:
            chapterName = "Chapter \(chapterResponse.data.number)"
        }
        
        chapter.id =  chapterResponse.data.id
        chapter.name = chapterName
        chapter.verseCount = Int16(chapterResponse.data.verseCount)
        chapter.nextId = chapterResponse.data.next?.id
        chapter.previousId = chapterResponse.data.previous?.id
        
        var lastId: String = ""
        var isFirstVerseOfChapter = true
        var prefix = ""
        
        var verseDictionary = [String: NSManagedObjectID]()
        
        chapterResponse.data.content.forEach { item in
            var verse = VerseEntity(context: privateMoc)
            var t: String = ""
            var isFirstVerseOfParagraph = true
            
            item.items.forEach { paragraphItem in
                switch paragraphItem {
                case .tag(let tag):
                    if tag.attrs.style == "v" {
                        // Normal verse
                    } else if tag.attrs.style == "q" {
                        // i.e. psalms
                    }
                case .text(let text):
                    if isFirstVerseOfChapter {
                        if loggingEnabled { print("Verse \(text.attrs.verseId) is the first verse.") }
                        prefix = ""
                        isFirstVerseOfChapter = false
                        isFirstVerseOfParagraph = false
                    } else if isFirstVerseOfParagraph {
                        if loggingEnabled { print("Verse \(text.attrs.verseId) is the start of a paragraph.") }
                        prefix = "\n\n"
                        isFirstVerseOfParagraph = false
                    } else if lastId != text.attrs.verseId /*&& lastId != "" */{
                        if loggingEnabled { print("Verse \(text.attrs.verseId) is part of a paragraph.") }
                        verse = VerseEntity(context: privateMoc)
                        t = ""
                        prefix = ""
                    } else {
                        if loggingEnabled { print("Verse \(text.attrs.verseId) again.") }
                    }
                    
                    t += text.text
                    
                    if let objectId = verseDictionary[text.attrs.verseId] {
                        guard let v = privateMoc.object(with: objectId) as? VerseEntity else {
                            print("No object found")
                            return
                        }
                        verse = v
                    } else {
                        verseDictionary[text.attrs.verseId] = verse.objectID
                    }
                    
                    verse.id = text.attrs.verseId
                    verse.text = t
                    verse.prefix = prefix
                    verse.chapter = chapter
                    
                    lastId = text.attrs.verseId
                    
                    do {
                        try privateMoc.save()
                    } catch {
                        print(error.localizedDescription)
                        return
                    }
                }
            }
        }
        
        do {
            try moc.save()
        } catch {
            print(error.localizedDescription)
            return nil
        }
        
        print("Saved \(chapter.verseEntityArray.count)/\(chapter.verseCount) verses in chapter \(chapter.number) of \(book.name)")
        
        return chapter.objectID
    }
}
