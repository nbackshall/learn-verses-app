//
//  VerseEntity+CoreDataProperties.swift
//  learn-verses
//
//  Created by Nicholas Backshall on 08/05/2022.
//
//

import Foundation
import CoreData


extension VerseEntity {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<VerseEntity> {
        return NSFetchRequest<VerseEntity>(entityName: "VerseEntity")
    }

    @NSManaged public var text: String
    @NSManaged public var id: String
    @NSManaged public var prefix: String
    @NSManaged public var chapter: ChapterEntity?
    
    public var number: Int {
        if let n = RegexHelper.getFirstMatch(string: id, regex: "([0-9]+)$") {
            return Int(n) ?? -2
        } else {
            return -1
        }
    }
}

extension VerseEntity : Identifiable {

}
