//
//  PersistenceController+Bible.swift
//  learn-verses
//
//  Created by Nicholas Backshall on 10/05/2022.
//

import Foundation
import CoreData

extension PersistenceController {
    func createBible(forId id: String, withBooks books: BooksResponse, moc: NSManagedObjectContext) {
        let privateMoc = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
        privateMoc.parent = moc
        
        let bible = BibleEntity(context: privateMoc)
        bible.id = id
        bible.name = "Dev Bible"
        bible.bookCount = Int16(books.data.count)

        books.data.enumerated().forEach { i, b in
            let book = BookEntity(context: privateMoc)
            book.id = b.id
            book.name = b.name
            book.chapterCount = Int16(b.chapters.count)
            book.position = Int16(i)
            book.bible = bible
            
            b.chapters.forEach { c in
                
                let chapterName: String
                switch book.id {
                case "PSA":
                    chapterName = "Psalm \(c.number)"
                case "PRO":
                    chapterName = "Proverb \(c.number)"
                default:
                    chapterName = "Chapter \(c.number)"
                }
                
                let chapter = ChapterEntity(context: privateMoc)
                chapter.id = c.id
                chapter.name = chapterName
                chapter.verseCount = 0
                chapter.book = book
                
                do {
                    try privateMoc.save()
                } catch {
                    print(error.localizedDescription)
                    return
                }
            }
        }
        
        do {
            try moc.save()
        } catch {
            print(error.localizedDescription)
            return
        }
        
        print("Saved bible with CoreData")
    }
}
