//
//  ChapterEntity+CoreDataProperties.swift
//  learn-verses
//
//  Created by Nicholas Backshall on 08/05/2022.
//
//

import Foundation
import CoreData


extension ChapterEntity {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<ChapterEntity> {
        return NSFetchRequest<ChapterEntity>(entityName: "ChapterEntity")
    }

    @NSManaged public var id: String
    @NSManaged public var verseCount: Int16
    @NSManaged public var name: String
    @NSManaged public var nextId: String?
    @NSManaged public var previousId: String?
    @NSManaged public var verses: NSOrderedSet
    @NSManaged public var book: BookEntity?
    
    public var number: Int {
        if let n = RegexHelper.getFirstMatch(string: id, regex: "([0-9]+)$") {
            return Int(n) ?? -2
        } else {
            return -1
        }
    }
    
    public var verseEntityArray: [VerseEntity] {
        let array = verses.array as? [VerseEntity] ?? []
        return array.sorted {
            $0.number < $1.number
        }
    }
    
    public var hasSavedAllVerses: Bool {
        verseCount == verseEntityArray.count && verseCount != 0
    }
    
    public var nextChapter: ChapterEntity? {
        guard let book = book else {
            print("Book is null, returning self")
            return nil
        }
        guard number > 0 else {
            print("Number is negative, returning self")
            return nil
        }
        let index = number - 1
        if index == (book.chapterCount - 1) {
            guard let nextBook = book.nextBook else {
                return nil
            }
            guard let nextChapter = nextBook.chapterEntityArray.first else {
                print("Array of chapters in new book is empty, returning self")
                return nil
            }
            return nextChapter
        } else {
            return book.chapterEntityArray[Int(index) + 1]
        }
    }
    
    public var previousChapter: ChapterEntity? {
        guard let book = book else {
            print("Book is null, returning self")
            return nil
        }
        guard number > 0 else {
            print("Number is negative, returning self")
            return nil
        }
        let index = number - 1
        if index == 0 {
            guard let previousBook = book.previousBook else {
                return nil
            }
            guard let previousChapter = previousBook.chapterEntityArray.last else {
                print("Array of chapters in new book is empty, returning self")
                return nil
            }
            return previousChapter
        } else {
            return book.chapterEntityArray[Int(index) - 1]
        }
    }
    
    public func isEqual(to other: ChapterEntity) -> Bool {
        return self.objectID === other.objectID
    }
}

// MARK: Generated accessors for verses
extension ChapterEntity {

    @objc(insertObject:inVersesAtIndex:)
    @NSManaged public func insertIntoVerses(_ value: VerseEntity, at idx: Int)

    @objc(removeObjectFromVersesAtIndex:)
    @NSManaged public func removeFromVerses(at idx: Int)

    @objc(insertVerses:atIndexes:)
    @NSManaged public func insertIntoVerses(_ values: [VerseEntity], at indexes: NSIndexSet)

    @objc(removeVersesAtIndexes:)
    @NSManaged public func removeFromVerses(at indexes: NSIndexSet)

    @objc(replaceObjectInVersesAtIndex:withObject:)
    @NSManaged public func replaceVerses(at idx: Int, with value: VerseEntity)

    @objc(replaceVersesAtIndexes:withVerses:)
    @NSManaged public func replaceVerses(at indexes: NSIndexSet, with values: [VerseEntity])

    @objc(addVersesObject:)
    @NSManaged public func addToVerses(_ value: VerseEntity)

    @objc(removeVersesObject:)
    @NSManaged public func removeFromVerses(_ value: VerseEntity)

    @objc(addVerses:)
    @NSManaged public func addToVerses(_ values: NSOrderedSet)

    @objc(removeVerses:)
    @NSManaged public func removeFromVerses(_ values: NSOrderedSet)

}

extension ChapterEntity : Identifiable {

}
