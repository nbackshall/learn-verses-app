//
//  PersistenceController+Verse.swift
//  learn-verses
//
//  Created by Nicholas Backshall on 08/05/2022.
//

import Foundation
import CoreData

extension PersistenceController {
    func savePassage(_ passage: PassageResponse, moc: NSManagedObjectContext) {
        
        let privateMoc = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
        privateMoc.parent = moc
        
        let book = BookEntity(context: privateMoc)
        book.id = "GEN"
        book.name = "Genesis"
        book.chapterCount = 0
        
        var chapter: ChapterEntity = ChapterEntity(context: privateMoc)
        
        var chapterIndex: Int = 0
        var prefix: String = ""
        
        passage.data.content.forEach { item in
            switch item {
            case .chapter(let c):
                chapter = ChapterEntity(context: privateMoc)
                chapter.id =  passage.data.chapterIds[chapterIndex]
                chapter.name = c.attrs.number
                chapter.book = book
                chapterIndex += 1
            case .paragraph(let p):
                var verse = VerseEntity(context: privateMoc)
                p.items.forEach { paragraphItem in
                    switch paragraphItem {
                    case .tag(let tag):
                        if tag.attrs.style == "v" {
                            verse = VerseEntity(context: privateMoc)
                            prefix += tag.attrs.number
                            prefix += " "
                        }
                    case .text(let text):
                        verse.id = text.attrs.verseId
                        verse.text = text.text
                        verse.prefix = prefix
                        verse.chapter = chapter
                        
                        do {
                            try privateMoc.save()
                        } catch {
                            print(error.localizedDescription)
                            return
                        }
                        
                        prefix = ""
                    }
                }
            }
        }
        
        do {
            try moc.save()
        } catch {
            print(error.localizedDescription)
            return
        }
    }
}
