//
//  learn_versesApp.swift
//  learn-verses
//
//  Created by Nicholas Backshall on 20/09/2020.
//

import SwiftUI

@main
struct learn_versesApp: App {
    @StateObject var viewModel: BookSelectionViewModel = BookSelectionViewModel()
    let persistenceController = PersistenceController.shared
    
    @Environment(\.scenePhase) var scenePhase
    
    var body: some Scene {
        WindowGroup {
            BookSelectionView(viewModel: viewModel)
                .environment(\.managedObjectContext, persistenceController.container.viewContext)
        }
        .onChange(of: scenePhase) { _ in
            persistenceController.save()
        }
    }
}
