//
//  Regex.swift
//  learn-verses
//
//  Created by Nicholas Backshall on 06/02/2022.
//

import Foundation

struct RegexHelper {
    static func getFirstMatch(string s: String, regex r: String) -> String? {
        let range = NSRange(location: 0, length: s.utf16.count)
        let regex = try! NSRegularExpression(pattern: r)
        if let match = regex.firstMatch(in: s, options: [], range: range) {
            let start = s.index(s.startIndex, offsetBy: match.range.location)
            let end = s.index(start, offsetBy: match.range.length)
            return String(s[start..<end])
        } else {
            return nil
        }
    }
}
