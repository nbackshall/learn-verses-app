//
//  BookViewHelper.swift
//  learn-verses
//
//  Created by Nicholas Backshall on 06/02/2022.
//

import SwiftUI

struct BookViewHelper {
    static let xMin: Double = 1.0
    static let xMax: Double = 50.0
    
    static func calculateHeight(chapterCount x: Int, size s: CGSize) -> CGFloat {
        let y = s.width
        let yMin = y * 0.08
        let yMax = y * 0.4
        return rectifiedLinearUnit(x: Double(x), xMax: xMax, xMin: xMin, yMax: yMax, yMin: yMin)
    }
    
    // needs work on minx
    private static func transformedSigmoid(x: Double, xMax: Double, xMin: Double, yMax: Double, yMin: Double) -> Double {
        return sigmoid(x: (Double(x) - xMax * 0.5) * 10 / xMax) * (yMax - yMin) + yMin
    }
    
    private static func sigmoid(x: Double) -> Double {
        return 1 / (1 + exp(-x))
    }
    
    // needs work on minx
    private static func rectifiedLinearUnit(x: Double, xMax: Double, xMin: Double, yMax: Double, yMin: Double) -> Double {
        if x <= xMin {
            return yMin
        } else if x >= xMax {
            return yMax
        } else {
            return x * (yMax - yMin) / xMax + yMin
        }
    }
}

extension BookViewHelper {
    static func color(from s: String) -> Color {
        let r, g, b: Double
        if let p = RegexHelper.getFirstMatch(string: s, regex: "[A-z]{3}") {
            let v = p.percentageThroughAlphabet()
            r = (v[0] + 1.0) / 2.0
            g = (v[1] + 1.0) / 2.0
            b = (v[2] + 1.0) / 2.0
        } else {
            r = 0.5
            g = 0.5
            b = 0.5
        }
        return Color(red: r, green: g, blue: b)
    }
}
