//
//  AnyCancellable+.swift
//  learn-verses
//
//  Created by Nicholas Backshall on 05/02/2022.
//

import Foundation
import Combine

typealias CancellableBag = Set<AnyCancellable>

extension CancellableBag {
    mutating func cancelAll() {
        forEach { $0.cancel() }
        removeAll()
    }
}
