//
//  ColorGenerator.swift
//  learn-verses
//
//  Created by Nicholas Backshall on 06/02/2022.
//

import Foundation
import SwiftUI

extension Color {
    public struct Book {
        static var pageLight: Color {
            return Color(red: 246.0 / 255.0, green: 245.0 / 255.0, blue: 232.0 / 255.0)
        }
        static var pageDark: Color {
            return Color(red: 225.0 / 255.0, green: 221.0 / 255.0, blue: 214.0 / 255.0)
        }
        
        struct Blue {
            static var main: Color {
                return Color(red: 79.0 / 255.0, green: 150.0 / 255.0, blue: 178.0 / 255.0)
            }
            static var highlight: Color {
                return Color(red: 102.0 / 255.0, green: 181.0 / 255.0, blue: 206.0 / 255.0)
            }
            static var shadow: Color {
                return Color(red: 86.0 / 255.0, green: 132.0 / 255.0, blue: 150.0 / 255.0)
            }
            static var detail: Color {
                return Color(red: 123.0 / 255.0, green: 174.0 / 255.0, blue: 193.0 / 255.0)
            }
        }
    }
}
