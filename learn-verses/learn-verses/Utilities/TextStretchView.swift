//
//  TextStretchView.swift
//  learn-verses
//
//  Created by Nicholas Backshall on 07/02/2022.
//

import SwiftUI

struct TextStretchView: View {
    @State private var frameWidth: CGFloat = 175
    @State private var frameHeight: CGFloat = 100
    
    @State private var textSize: CGSize = CGSize(width: 100, height: 100)
    
    
    private var sy = CGFloat(4.0)//height / Font.custom("Baskerville", size: 1000)
    private var sx = CGFloat(1.0)//width / ((UIFont.preferredFont(forTextStyle: .custom("Baskerville", size: 1000)).pointSize * CGFloat(title.count)))
    
    var body: some View {
        VStack {
            Text("Text: \(textSize.height); Frame: \(frameHeight)")
            FrameAdjustingContainer(frameWidth: $frameWidth, frameHeight: $frameHeight) {
                HStack(alignment: .center) {
                    Text("text")
                        .font(.custom("Baskerville", size: 400))
                        .textCase(.uppercase)
//                        .fixedSize() // Prevents text truncating
//                        .background(
//                            GeometryReader { (geo) -> Color in
//                                DispatchQueue.main.async {  // hack for modifying state during view rendering.
//                                    textSize = geo.size
//                                }
//                                return Color.clear
//                            }
//                        )
                        .fixedSize()
//                        .minimumScaleFactor(0.01)  // 2
                        .lineLimit(1)  // 3
                        .border(Color.blue, width: 1)
//                        .scaleEffect(min(frameWidth / textSize.width, 1.0))  // making view smaller to fit the frame.
//                        .scaleEffect(CGSize(width: frameWidth / textSize.width, height: frameHeight / textSize.height))
                        .scaleEffect(CGSize(
                            width: frameWidth / ("TEXT".widthOfString(usingFont: UIFont(name: "Baskerville", size: 400)) ?? frameWidth),
                            height: frameHeight / ("TEXT".heightOfString(usingFont: UIFont(name: "Baskerville", size: 400)) ?? frameHeight)
                        ))
//                                        .scaleEffect(CGSize(width: 1.0, height: textSize.width <= frameWidth ? frameHeight / textSize.height : 1.0)  // making view smaller to fit the frame.
//                        .scaleEffect(min(1.0, 100/23))  // making view smaller to fit the frame.
                }
            }
            FrameAdjustingContainer(frameWidth: $frameWidth, frameHeight: $frameHeight) {
                HStack(alignment: .center) {
                    Text("texter")
                        .font(.custom("Baskerville", size: 400))  // 1
                        .textCase(.uppercase)
                        .minimumScaleFactor(0.01)  // 2
                        .lineLimit(1)  // 3
                        .border(Color.blue, width: 1)
//                        .scaleEffect(CGSize(width: 1.0, height: frameHeight/35.0))
//                        .scaleEffect(CGSize(width: frameWidth / "texter".heightOfString(usingFont: Font.custom("Baskerville", size: 400)), height: frameHeight / "texter".widthOfString(usingFont: Font.custom("Baskerville", size: 400))))
                        .scaleEffect(CGSize(
                            width: 1.0,
                            height: frameHeight / ("texter".widthOfString(usingFont: UIFont(name: "Baskerville", size: 40)) ?? frameHeight)
                        ))
                }
            }
        }
    }
}

struct TextStretchView_Previews: PreviewProvider {
    static var previews: some View {
        TextStretchView()
    }
}

struct FrameAdjustingContainer<Content: View>: View {
    @Binding var frameWidth: CGFloat
    @Binding var frameHeight: CGFloat
    let content: () -> Content
    
    var body: some View  {
        ZStack {
            content()
                .frame(width: frameWidth, height: frameHeight)
                .border(Color.red, width: 1)
            
            VStack {
                Spacer()
                Slider(value: $frameWidth, in: 50...300)
                Slider(value: $frameHeight, in: 50...600)
            }
            .padding()
        }
    }
}
