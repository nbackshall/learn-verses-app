//
//  String+.swift
//  learn-verses
//
//  Created by Nicholas Backshall on 06/02/2022.
//

import Foundation
import UIKit

extension String {
    func percentageThroughAlphabet() -> [Double] {
        return Array(self.lowercased().utf8).map { Double($0 - 96) / 26.0 }
    }
    
    func heightOfString(usingFont font: UIFont?) -> CGFloat? {
        guard font != nil else { return nil }
        let fontAttributes = [NSAttributedString.Key.font: font]
        let size = self.size(withAttributes: fontAttributes as [NSAttributedString.Key : Any])
        return size.height
    }

    func widthOfString(usingFont font: UIFont?) -> CGFloat? {
        guard font != nil else { return nil }
        let fontAttributes = [NSAttributedString.Key.font: font]
        let size = self.size(withAttributes: fontAttributes as [NSAttributedString.Key : Any])
        return size.width
    }
    
    func appendLineToURL(fileURL: URL) throws {
        try (self + "\n").appendToURL(fileURL: fileURL)
    }

    func appendToURL(fileURL: URL) throws {
        let data = self.data(using: String.Encoding.utf8)!
        try data.append(fileURL: fileURL)
    }
}
