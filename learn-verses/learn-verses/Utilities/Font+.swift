//
//  Font+.swift
//  learn-verses
//
//  Created by Nicholas Backshall on 14/05/2022.
//

import Foundation
import SwiftUI

enum BibleTextTypes {
    case bible
    case book
    case chapter
    case verse
    case verseNumber
    
    var font: UIFont? {
        switch self {
        case .bible:
            return UIFont(name: Font.selectedBookFont, size: Font.bibleFontSize)
        case .book:
            return UIFont(name: Font.selectedBookFont, size: Font.bookFontSize)
        case .chapter:
            return UIFont(name: Font.selectedChapterFont, size: Font.chapterFontSize)
        case .verse:
            return UIFont(name: Font.selectedVerseFont, size: Font.verseFontSize)
        case .verseNumber:
            return UIFont(name: Font.selectedVerseNumberFont, size: Font.verseNumberFontSize)
        }
    }
}

extension Font {
    static let publicoFont = UIFont(name: "PublicoText", size: UIFont.labelFontSize)
    
    static let bibleFontSize: CGFloat = 40
    static let bookFontSize: CGFloat = 40
    static let chapterFontSize: CGFloat = 22
    static let verseFontSize: CGFloat = 18
    static let verseNumberFontSize: CGFloat = 12
    
    static let selectedBibleFont = "PublicoText-Roman"
    static let selectedBookFont = "PublicoText-Semibold"
    static let selectedChapterFont = "PublicoText-Semibold"
    static let selectedVerseFont = "PublicoText-Roman"
    static let selectedVerseNumberFont = "PublicoText-Roman"
    
    init(forType type: BibleTextTypes) {
        self = Font(type.font ?? UIFont.systemFont(ofSize: 40))
    }
    
    static var publicoVerseNumberBaselineOffset: CGFloat {
        (BibleTextTypes.verse.font?.capHeight ?? 0) * 0.6
    }
    
    static var bookNameLineWidth: CGFloat {
        (BibleTextTypes.verse.font?.capHeight ?? 0) * 6
    }
}
