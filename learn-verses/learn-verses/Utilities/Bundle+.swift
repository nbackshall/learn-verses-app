//
//  Main\.swift
//  learn-verses
//
//  Created by Nicholas Backshall on 08/05/2022.
//

import Foundation
import Combine

extension Bundle{
    func readFile(file: String, withExtension: String?) -> AnyPublisher<Data, BibleError> {
        self.url(forResource: file, withExtension: withExtension)
            .publisher
            .tryMap{ string in
                print(string)
                guard let data = try? Data(contentsOf: string) else {
                    fatalError("Failed to load \(file) from bundle.")
                }
                return data
            }
            .mapError { error in
                return .noFile(description: "Failed to load \(file) from bundle.")
            }
            .eraseToAnyPublisher()
    }
    
    func decodeable<T: Decodable>(filename: String, withExtension: String?) -> AnyPublisher<T, BibleError> {
        readFile(file: filename, withExtension: withExtension)
            .decode(type: T.self, decoder: JSONDecoder())
            .mapError { error in
                return .parsing(description: "Failed while parsing \(filename)")
            }
            .eraseToAnyPublisher()
        
    }
}
