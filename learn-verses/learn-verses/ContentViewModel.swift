//
//  ContentViewModel.swift
//  learn-verses
//
//  Created by Nicholas Backshall on 20/09/2020.
//

import Foundation
import Combine

class ContentViewModel: ObservableObject {
    
    @Published var biblesDataSource: [Bible] = []
    
    private let bibleFetcher: BibleFetchable
    private var disposables = Set<AnyCancellable>()
    
    init(bibleFetcher: BibleFetchable) {
        self.bibleFetcher = bibleFetcher
        fetchBibles()
    }
    
    func fetchBibles() {
        bibleFetcher.bibles()
            .map { response in
                response.data.map(Bible.init)
            }
            .receive(on: DispatchQueue.main)
            .sink(
              receiveCompletion: { [weak self] value in
                print ("Received completion: \(value).")
                guard let self = self else { return }
                switch value {
                case .failure(let error):
                    self.biblesDataSource = []
                    print("error: \(error)")
                case .finished:
                    break
                }
              },
              receiveValue: { [weak self] bibles in
                guard let self = self else { return }
                print ("Received user: \(bibles).")
                self.biblesDataSource = bibles
            })
            .store(in: &disposables)
    }
}
